# Configuration fon qemu-web-desktop

# WHERE THINGS ARE -------------------------------------------------------------

# name of service, used as directory and e.g. http://127.0.0.1/desktop
$config{service}                  = "qemu-web-desktop";

# full path to Apache HTML root
#   Apache default on Debian is /var/www/html
$config{dir_html}                 = "/usr/share/qemu-web-desktop/html"   ;    

# full path to root of the service area
#   e.g. /var/www/html/desktop
$config{dir_service}              = "/var/lib/$config{service}";

# full path to machines (ISO,VM)
$config{dir_machines}             = "$config{dir_service}/machines";

# full path to snapshots and temporary files
$config{dir_snapshots}            = "$config{dir_service}/snapshots";

# full path to snapshot config/lock files. Must NOT be accessible from http://
#   e.g. /tmp to store "desktop_XXXXXXXX.json" files
#
# NOTE: apache has a protection in:
#   /etc/systemd/system/multi-user.target.wants/apache2.service
#   PrivateTmp=true
# which creates a '/tmp' in e.g. /tmp/systemd-private-*-apache2.service-*/
$config{dir_cfg}                  = File::Spec->tmpdir(); 

# full path to noVNC, e.g. "/usr/share/novnc". Must contain "vnc.html"
$config{dir_novnc}                = "/usr/share/novnc";
# websockify command, e.g. "/usr/bin/websockify"
$config{dir_websockify}           = "websockify";


# set a list of mounts to export into VMs.
# these are tested for existence before mounting. The QEMU mount_tag is set to 
# the last word of mount path prepended with 'host_'.
# Use e.g. `mount -t 9p -o trans=virtio,access=client host_media /mnt/media` in guest.
my @mounts                        = ('/mnt','/media');
$config{dir_mounts}               = [@mounts];

# MACHINE DEFAULT SETTINGS -----------------------------------------------------

# max session life time in sec. 1 day is 86400 s. Highly recommended.
#   Use 0 to disable (infinite)
$config{snapshot_lifetime}        = 86400; 

# default nb of CPU per session.
$config{snapshot_alloc_cpu}       = 1;

# default nb of RAM per session (in MB).
$config{snapshot_alloc_mem}       = 4096.0;

# default size of disk per session (in GB). Only for ISO machines.
$config{snapshot_alloc_disk}      = 10.0;

# default machine to run
$config{machine}                  = 'slax.iso';

# QEMU executable. Adapt to the architecture you run on.
$config{qemu_exec}                = "qemu-system-x86_64";

# QEMU video driver, can be "qxl" or "vmware"
$config{qemu_video}               = "qxl"; 

# search detached GPU (via vfio-pci). Only use video part, no audio.
{
  my ($device_pci, $device_model, $device_name) = pci_devices("lspci -nnk","vga","vfio");
  if (not @$device_pci) {
    ($device_pci, $device_model, $device_name) = pci_devices("lspci -nnk","controller","vfio");
  }
  $config{gpu_model}             = [@$device_model];
  $config{gpu_name}              = [@$device_name];
  $config{gpu_pci}               = [@$device_pci];
}

# SERVICE CONTRAINTS -----------------------------------------------------------

# max amount [0-1] of CPU load. Deny service when above.
$config{service_max_load}         = 0.8  ;

# max number of active sessions. Deny service when above.
$config{service_max_session_nb}   = 10;

# allow re-entrant sessions. Safer with single-shot.
#   0: non-persistent (single-shot) are lighter for the server, but limited in use.
#   1: persistent sessions can be re-used within life-time until shutdown.
$config{service_allow_persistent} = 1;

# when set (non 0), a reduced port range is used as 
#   [service_min_port service_min_port+service_max_session_nb]
# else (when 0) a random port is found in 0-65535
$config{service_min_port}         = 6001;

# USER AUTHENTICATION ----------------------------------------------------------

# must use token to connect (highly recommended)
#   when false, no token is used (direct connection).
$config{service_use_vnc_token}    = 1;

# the name of the SMTP server, and optional port.
#   when empty, no email is needed, token is shown.
#   The SMTP server is used to send emails, and check user credentials.
$config{smtp_server}              = "smtp.synchrotron-soleil.fr"; 

# the SMTP port e.g. 465, 587, or left blank
# and indicate if SMTP uses encryption
$config{smtp_port}                = 587; 
$config{smtp_use_ssl}             = 'starttls'; # 'starttls' or blank

# the name of the IMAP server, and optional port.
#   when empty, no email is needed, token is shown.
#   The IMAP server is used to check user credentials.
$config{imap_server}              = 'sun-owa.synchrotron-soleil.fr'; 

# the IMAP port e.g. 993, or left blank
$config{imap_port}                = 993; 

# the name of the LDAP server.
#   The LDAP server is used to check user credentials.
$config{ldap_server}              = '195.221.10.1'; 
$config{ldap_port}                = 389;    # default is 389
$config{ldap_domain}              = 'EXP';  # DC

# the email address of the sender of the messages on the SMTP server. 
$config{email_from}               = 'luke.skywalker@synchrotron-soleil.fr';

# the password for the sender on the SMTP server, or left blank when none.
$config{email_passwd}             = "";

# the method to use for sending messages. Can be:
#   auto    use the provided smtp/email settings to decide what to do
#   SSL     use the SMTP server, port SSL, and email_from with email_passwd
#   port    just use the server with given SMTP port
#   simple  just use the server, and port 25
$config{email_method}             = "simple";

# how to check users

# the email authentication is less secure. Use it with caution.
#   the only test is for an "email"-like input, but not actual valid / registered email.
#   When used, you MUST make sure $config{service_use_vnc_token} = 1
#   When authenticated with email, only single-shot sessions can be launched.
$config{check_user_with_email}    = 0;  # send token via email.
$config{check_user_with_imap}     = 0;  

# In case of IMAP error "Unable to connect to <server>: SSL connect attempt 
# failed error:1425F102:SSL routines:ssl_choose_client_version:unsupported protocol.
# See:
# https://stackoverflow.com/questions/53058362/openssl-v1-1-1-ssl-choose-client-version-unsupported-protocol

$config{check_user_with_smtp}     = 0;
$config{check_user_with_ldap}     = 0;

# set the list of 'admin' users that can access the Monitoring pages.
# these must also be identified with their credentials.
my @admin = ('picca','farhie','roudenko','bac','ounsy','bellachehab');
$config{user_admin} = [@admin];

# KEEP THIS LINE!
1;
