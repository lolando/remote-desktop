% QWDCTL(1)
% Roland Mas
% April 2021

# NAME

qwdctl - Control qemu-web-desktop

# SYNOPSIS

**qwdctl** *keyword*

# DESCRIPTION

**qwdctl** is a simple script that downloads virtual machine images
for use with **qemu-web-desktop** and/or refreshes the list of
available images so that **qemu-web-desktop** can display it in its
web interface. 

Each entry in the configuration file `/etc/qemu-web-desktop/machines.conf` 
spans on 3 lines:

-  [short_name] 
-  url=[URL to ISO, QCOW2, VDI, VMDK, RAW virtual machine disk]
-  description=[description to be shown in the login page] 

Images listed in the configuration file without a `url=` parameter are
expected to be downloaded by hand and installed into
`/var/lib/qemu-web-desktop/machines` by the local administrator. Then, just 
specify the [name] and description.


# SUBCOMMANDS

**download**
:   Downloads virtual machine images referenced in the
    /etc/qemu-web-desktop/machines.conf file. A **refresh** is automatically
    launched afterwards.

**refresh**
:   Regenerate the list of available images based on the
    /etc/qemu-web-desktop/machines.conf file. 
    The generated file is e.g. `/var/lib/qemu-web-desktop/include.html` which is 
    linked as `/usr/share/qemu-web-desktop/html/desktop/machines.html`.

